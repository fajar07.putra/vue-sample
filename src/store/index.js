import Vue from 'vue'
import Vuex from 'vuex'
import postModule from './postModule'
import postGetter from './postGetter'

Vue.use(Vuex);
const store = new Vuex.Store({
    modules: {
        postModule: postModule
    },
    getters: postGetter
});

export default store