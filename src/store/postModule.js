import PostResource from '@/api/PostResource'

const postResource = new PostResource()
const state = {
    list: [],
    form: {
        text: ''
    }
}

const mutations = {
    ASSIGN_LIST: (state, payload) => {
        // console.log('payload', payload)
        state.list = payload.map(post => {
            post.createdAt = new Date(post.createdAt)
            return post
        });
    },
    CLEAR_INPUT: (state) => {
        state.form.text = ''
    },
    DELETE_LIST: (state, id) => {
        state.list = state.list.filter(fltr => {
            fltr._id != id
        });
    }
}

const actions = {
    getList({ commit }) {
        return new Promise((resolve, reject) => {
            postResource.list({}).then(response => {
                commit('ASSIGN_LIST', response.data)
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    storePost({ commit, state }) {
        return new Promise((resolve, reject) => {
            postResource.store(state.form)
                .then(() => {
                    commit('CLEAR_INPUT')
                    resolve()
                })
                .catch(error => {
                    console.log(error)
                    reject(error)
                })
        })
    },

    deletePost(obj, id) {
        // console.log(id)
        return new Promise((resolve, reject) => {
            postResource.destroy(id)
                .then(() => {
                    // obj.commit('DELETE_LIST', id)
                    resolve()
                })
                .catch(error => {
                    console.log(error)
                    reject(error)
                })
        })
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}