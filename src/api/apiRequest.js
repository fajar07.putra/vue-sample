import axios from 'axios'

// Create axios instance
const service = axios.create({
    baseURL: 'http://localhost:5000',
    timeout: 10000 // Request timeout
})

export default service