import apiRequest from './apiRequest'

class PostResource {

    list(query) {
        return apiRequest({
            url: '/api/posts',
            method: 'get',
            params: query
        })
    }
    store(resource) {
        return apiRequest({
            url: '/api/posts',
            method: 'post',
            data: resource
        })
    }
    destroy(id) {
        return apiRequest({
            url: '/api/posts/' + id,
            method: 'delete'
        })
    }
}

// export { PostResource as default }
export default PostResource